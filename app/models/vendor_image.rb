class VendorImage < ActiveRecord::Base
  belongs_to    :vendors_wines

  has_attached_file :image
end
