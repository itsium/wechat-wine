class Wine < ActiveRecord::Base
  has_many                      :images

  accepts_nested_attributes_for :images

  def title
    "#{appellation} #{vintage} #{varietal} #{producer} #{vineyard}"
  end

  def as_json(options = {})
    super(options).tap do |json|
      json[:images] = images.map(&:image).map(&:url)
    end
  end
end
