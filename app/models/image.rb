class Image < ActiveRecord::Base
  belongs_to    :wine

  has_attached_file :image

  def as_json(options = {})
    super(options).tap do |json|
      json[:url] = image.map(&:image).map(&:url)
    end
  end
end
