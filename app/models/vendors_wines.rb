class VendorsWines < ActiveRecord::Base
  belongs_to    :vendor
  belongs_to    :wine

  validates :vendor, :wine, presence: true
  validates :title, presence: true

  has_many :images, through: :wine
  has_many :vendor_images

  def as_json(options = {})
    super(options).tap do |json|
      json[:wine] = wine
      json[:vendor] = vendor
      json[:vendor_images] = vendor_images.map(&:image).map(&:url)
    end
  end
end
