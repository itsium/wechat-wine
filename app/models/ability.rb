class Ability
  include CanCan::Ability

  def initialize(user)
    can :access, :rails_admin
    can :dashboard
    can :index, VendorsWines, vendor_id: user.id
    can :new_vendor, VendorsWines, vendor_id: user.id
    can :show, VendorsWines, vendor_id: user.id
    can :edit, VendorsWines, vendor_id: user.id
    can :destroy, VendorsWines, vendor_id: user.id

    can :show, Wine
    can :index, Wine
    can :add, Wine

    can :show, Image

    can :manage, VendorImage
    can :edit, Vendor, id: user.id
    can :show, Vendor, id: user.id
    can :index, Vendor, id: user.id
  end
end
