class VendorsWinesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def show
    render json: VendorsWines.find(params[:id]), callback: params['callback']
  end
end
