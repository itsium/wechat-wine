RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :vendor
  end
  config.current_user_method(&:current_vendor)

  ## == Cancan ==
  config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.model Vendor do
    edit do
      field :email
      field :phone
    end
  end

  config.model VendorsWines do
    edit do
      field :wine
      field :title
      field :description
      field :vendor_id, :hidden do
        default_value do
          bindings[:view]._current_user.id
        end
      end

      configure :vendor do
        visible false
      end
    end

    list do
      field :wine
    end
  end

  config.model Wine do
    list do
      configure :external_id do
        visible false
      end
    end
    object_label_method { :title }
  end

  require 'rails_admin/config/actions/new_vendor'

  config.actions do
    dashboard                     # mandatory
    index
    new_vendor

    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
