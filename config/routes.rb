Rails.application.routes.draw do
  mount RailsAdmin::Engine => '//shop', as: 'rails_admin'

  resources :vendors_wines, only: [:show]

  devise_for :vendors
  devise_scope :vendor do
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  root 'devise/sessions#new'
end
