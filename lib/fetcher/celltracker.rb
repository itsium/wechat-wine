module Fetcher
  class CellTracker
    BASE_URL = "http://www.cellartracker.com"
    USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.%s Safari/537.36"
    FILE_ROOT = "http://ct-static.com/labels/%s.jpg"

    def self.fetch!
      fetch_region!("bordeaux")
    end

    def self.fetch_region!(region)
      puts "Fetch region '#{region}..."
      search_region(region) do |wine|
        fetch_wine! wine
      end
    end

    def self.fetch_wine!(wine)
      puts "Fetch wine #{wine}"
      if Wine.where(external_id: wine).empty?
        Wine.create search_wine(wine)
      end
    rescue => error
      puts "#{error}\n#{error.backtrace}"
    end

    private
    def self.search_region(region)
      for page in 8..Float::INFINITY
        puts "Get page #{page}..."
        response = get "list.asp", region: region, page: page
        wines = response.css(".winelist > tr")[1..-1]
        unless wines
          puts "Unable to get wines list, response was: #{response.to_xml}"
        else
          wines.each do |wine|
            yield wine.css(".more").first['href'].match(/iWine=(.+)/)[1]
          end
          break if wines.size < 25
        end
      end
    end

    def self.search_wine(wine)
      response = get "wine.asp", iWine: wine
      flat_attributes =
        response.css("#tab_three > ul > li").map do |attr|
          norm_attribute = attr.children.first.text.downcase
          if Wine.new.attributes.keys.include? norm_attribute
            [norm_attribute.to_sym, attr.css('span').text]
          end
      end.compact
      {external_id: wine}.merge(Hash[flat_attributes]).tap do |attributes|
        # replace all 'n/a' by nil
        attributes.map do |attribute, value|
          attributes[attribute] = nil if value == 'n/a'
        end

        # Get all images
        if response.css(".wine_photo_empty").empty?
          photo_number = response.css(".photo_number").text.match(/of (.+)/)[1].to_i
          attributes[:images_attributes] =
            [1..photo_number].map do |label|
            { image: get_photo(wine, label) }
          end
        end

        attributes[:kind] = attributes.delete(:type) # :type is reserved

        puts "Create wine: #{attributes.inspect}"
      end
    end

    def self.get_photo(wine, label)
      response = get "ajaxlabel.asp", {label: label, iwine: wine}
      FILE_ROOT % JSON.parse(response)['iLabel']
    end

    def self.get(path, params)
      puts "GET #{path} with #{params.inspect}"
      response = RestClient.get(File.join(BASE_URL, path), {
                       params: params,
                       user_agent: user_agent,
                       cookies: {
                         'PRLST' =>  'ob',
                         'adOtr' =>  'aW7E6',
                         'SPSI'  =>  '67e2a0045bdfef6300e3cac67efb95b0',
                         'UTGv2' =>  'h402d5645ddc487aa2527b78121902bfd258'
                       }
                     })
      Nokogiri::HTML(response)
    end

    def self.user_agent
      USER_AGENT % rand(999)
    end
  end
end
