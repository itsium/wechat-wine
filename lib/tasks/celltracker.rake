require 'fetcher/celltracker'

namespace :celltracker do
  task :fetch => :environment do |t, args|
    Fetcher::CellTracker.fetch!
  end
end
