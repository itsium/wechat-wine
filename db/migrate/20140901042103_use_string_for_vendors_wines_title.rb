class UseStringForVendorsWinesTitle < ActiveRecord::Migration
  def change
    change_column :vendors_wines, :title, :string
  end
end
