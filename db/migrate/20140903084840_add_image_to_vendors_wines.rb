class AddImageToVendorsWines < ActiveRecord::Migration
  def change
    add_column :vendors_wines, :image_id, :integer
    add_index :vendors_wines, :image_id
  end
end
