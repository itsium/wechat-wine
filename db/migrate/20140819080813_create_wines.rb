# -*- coding: utf-8 -*-
#{:id=>"1662741", "vintage"=>"2004", "type"=>"Red", "producer"=>"1938", "varietal"=>"Red Bordeaux Blend", "designation"=>"n/a", "vineyard"=>"Pas les Producteurs Réunis", "country"=>"France", "region"=>"Bordeaux", "subregion"=>"Libournais", "appellation"=>"Lussac-St. Émilion"}

class CreateWines < ActiveRecord::Migration
  def change
    create_table :wines do |t|
      t.string  :external_id, uniqueness: true
      t.integer :vintage
      t.string  :type
      t.string  :producer
      t.string  :varietal
      t.string  :designation
      t.string  :vineyard
      t.string  :country
      t.string  :region
      t.string  :subregion
      t.string  :appellation

      t.timestamps
    end
  end
end
