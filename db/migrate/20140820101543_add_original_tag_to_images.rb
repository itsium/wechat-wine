class AddOriginalTagToImages < ActiveRecord::Migration
  def change
    add_column :images, :original, :boolean, default: false
  end
end
