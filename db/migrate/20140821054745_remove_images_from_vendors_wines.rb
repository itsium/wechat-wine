class RemoveImagesFromVendorsWines < ActiveRecord::Migration
  def change
    remove_column :vendors_wines, :image_id
  end
end
