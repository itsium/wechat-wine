class AddWinesVendors < ActiveRecord::Migration
  def change
    create_table :wines_vendors do |t|
      t.references      :wine,          index: true
      t.references      :vendor,        index: true
      t.references      :image,         index: true

      t.text            :description
    end
  end
end
