class CreateVendorImages < ActiveRecord::Migration
  def change
    create_table :vendor_images do |t|
      t.belongs_to :vendors_wines
      t.attachment      :image
      t.timestamps
    end
  end
end
