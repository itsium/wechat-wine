class RenameWinesVendorsToVendorsWines < ActiveRecord::Migration
  def change
    rename_table :wines_vendors, :vendors_wines
  end
end
