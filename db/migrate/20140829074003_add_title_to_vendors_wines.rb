class AddTitleToVendorsWines < ActiveRecord::Migration
  def change
    add_column :vendors_wines, :title, :text
  end
end
