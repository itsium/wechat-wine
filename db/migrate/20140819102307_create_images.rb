class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.references      :wine, index: true
      t.attachment      :image

      t.timestamps
    end
  end
end
