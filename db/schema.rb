# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140903090607) do

  create_table "images", force: true do |t|
    t.integer  "wine_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "original",           default: false
  end

  add_index "images", ["wine_id"], name: "index_images_on_wine_id"

  create_table "vendor_images", force: true do |t|
    t.integer  "vendors_wines_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vendors", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
  end

  add_index "vendors", ["email"], name: "index_vendors_on_email", unique: true
  add_index "vendors", ["reset_password_token"], name: "index_vendors_on_reset_password_token", unique: true

  create_table "vendors_wines", force: true do |t|
    t.integer "wine_id"
    t.integer "vendor_id"
    t.text    "description"
    t.string  "title"
    t.integer "image_id"
  end

  add_index "vendors_wines", ["image_id"], name: "index_vendors_wines_on_image_id"
  add_index "vendors_wines", ["vendor_id"], name: "index_vendors_wines_on_vendor_id"
  add_index "vendors_wines", ["wine_id"], name: "index_vendors_wines_on_wine_id"

  create_table "wines", force: true do |t|
    t.string   "external_id"
    t.integer  "vintage"
    t.string   "kind"
    t.string   "producer"
    t.string   "varietal"
    t.string   "designation"
    t.string   "vineyard"
    t.string   "country"
    t.string   "region"
    t.string   "subregion"
    t.string   "appellation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
